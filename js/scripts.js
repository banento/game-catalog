$(document).ready ( function (){


    function getGame(url) {
        let result;
        return new Promise(resolve => {
            $.ajax({
        		url   : url,
        		// data   : {'key' : '36aac5b975144102a260d3508ee33476', dates: '2019-09-01,2019-09-30', platforms: '18,1,7'},
        		type  : "GET",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success : function(result){
                    resolve(result);
                }
            });
        });
    }

   let data_game = '';
   let next_url = '';

   let url ="https://api.rawg.io/api/games?key=36aac5b975144102a260d3508ee33476&dates=2019-09-01,2019-09-30&platforms=18,1,7";

   setContent();

   async function setContent(showmore= false){

       	if(showmore){
       		data_game = await getGame(next_url);
       	}else{
       		data_game = await getGame(url);
       	}
   		next_url = data_game.next;

   		let result = data_game.results;
   		let content = '';

   		let data_store = [];

   		for (var i = 0; i < result.length; i++) {

   			let game = result[i];

   			let bg_img = game.background_image;
   			let name = game.name;
   			let rating = game.rating_top;
   			let slug = game.slug;
   			let added = game.added;

   			data_store.push({'slug' : slug, 'added' : added});

   			content += ` 
        	<div class="col-md-4 col-lg-3" style="padding-bottom:15px">
	        	<div class="card">
				  <img src="`+ bg_img +`" class="card-img-top">
				  <div class="card-body">
				    <h5 class="card-title">`+ name +`</h5>
				    <div class="row">
				    	<div class="col-6">
				    		<p class="rating d-inline">`+ rating +`</p>
				    	</div>
				    	<div class="col-6 text-right">
				    		<a href="javascript:void(0)" class="btn btn-primary btn-sm plus_btn" data-id="`+ slug +`">+</a>
				    		<a href="javascript:void(0)" class="btn btn-danger btn-sm min_btn" data-id="`+ slug +`">-</a>
				    	</div>
				    	<div class="col-12 text-right">
				    		<span id="added_`+ slug +`" class="text-info">`+ added +`</span>
				    	</div>
				    </div>

				  </div>
				</div>
			</div>
			`;

   		}

   		/*let store = await store_data(data_store, 'add');

   		console.log(store);
*/
   		$("#content").append(content);

   }

	$('#showmore').on( 'click', function () {
		setContent(true)
	});

	$(document).on("click","a.min_btn", function () {
		let id = $(this).attr("data-id")

		let get_added = $("#added_"+id).text();

		get_added = parseInt(get_added);
		get_added--;
		$("#added_"+id).text(get_added);

	});
	$(document).on("click","a.plus_btn", function () {
			let id = $(this).attr("data-id")

			let get_added = $("#added_"+id).text();

			get_added = parseInt(get_added);
			get_added++;
			$("#added_"+id).text(get_added);
	});

	function store_data(data, category) {
        let result;
        return new Promise(resolve => {
            $.ajax({
        		url   : 'http://localhost/eiger/process.php',
        		// url   : 'http://localhost/eiger/app/Games.php',
        		type  : "POST",
        		method  : "POST",
    			data   : {'category' : category, 'data' : data},
			   	dataType: 'json',
                success : function(result){
                    console.log(result);
                }
            });
        });
    }


	$(document).on('scroll', function() {
	  if ($(this).scrollTop() >= $('#showmore').position().top) {
	    console.log('I have been reached');
	  }
	});

})